 -- First trigger --

DELIMITER //
DROP TRIGGER IF EXISTS clients_count_insert;


CREATE TRIGGER clients_count_insert AFTER
INSERT ON `clients`
FOR EACH ROW BEGIN
UPDATE `bank`
SET clients_count = clients_count + 1
WHERE (bank_id = 1); END;

// //
DROP TRIGGER IF EXISTS clients_count_delete;


CREATE TRIGGER clients_count_delete AFTER
DELETE ON `clients`
FOR EACH ROW BEGIN
UPDATE `bank`
SET clients_count = clients_count - 1
WHERE (bank_id = 1) ; END;

// //
DROP TRIGGER IF EXISTS workers_count_insert;


CREATE TRIGGER workers_count_insert AFTER
INSERT ON `workers`
FOR EACH ROW BEGIN
UPDATE `bank`
SET workers_count = workers_count + 1
WHERE (bank_id = 1); END;

// //
DROP TRIGGER IF EXISTS workers_count_delete;


CREATE TRIGGER workers_count_delete AFTER
DELETE ON `workers`
FOR EACH ROW BEGIN
UPDATE `bank`
SET workers_count = workers_count - 1
WHERE (bank_id = 1) ; END;

// -- End of first trigger --
 -- Second trigger --
 //
DROP TRIGGER IF EXISTS operations_history_date_check;


CREATE TRIGGER operations_history_date_check
BEFORE
INSERT ON `operations_history`
FOR EACH ROW BEGIN DECLARE last_date datetime;
SET last_date =
  (SELECT date_time
   FROM operations_history
   ORDER BY operation_id DESC
   LIMIT 1); IF (new.date_time <= last_date) THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Неверная дата в истории операций.'; END IF; END;

// //
DROP TRIGGER IF EXISTS quotes_date_check;


CREATE TRIGGER quotes_date_check
BEFORE
INSERT ON `quotes`
FOR EACH ROW BEGIN DECLARE last_date datetime;
SET last_date =
  (SELECT date_time
   FROM quotes
   ORDER BY quote_id DESC
   LIMIT 1); IF (new.date_time <= last_date) THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Неверная дата котировки.'; END IF; END // //
DROP TRIGGER IF EXISTS deposits_date_check;


CREATE TRIGGER deposits_date_check
BEFORE
INSERT ON `investment_in_deposits`
FOR EACH ROW BEGIN DECLARE MAX int;
SELECT max_date INTO MAX
FROM deposits
WHERE deposit_id = new.deposit_id; IF (datediff(new.date_of_finish, new.date_of_start) > MAX) THEN SIGNAL SQLSTATE '45000'
  SET MESSAGE_TEXT = 'Срок вложения в депозит превышает максимально допустимый.'; END IF; END;

// -- End of Second Trigger --
 -- Third Trigger --
//
DROP TRIGGER IF EXISTS investment_in_deposits_insert;


CREATE TRIGGER investment_in_deposits_insert AFTER
INSERT ON `investment_in_deposits`
FOR EACH ROW BEGIN
INSERT INTO operations_history (`write-off_account`, OPERATION, date_time, SUM, id_of_operation) value(new.`write-off_account`, 'investment_in_deposits', now(), new.sum, new.investment_id); END;

// //
DROP TRIGGER IF EXISTS investment_in_securuties_insert;


CREATE TRIGGER investment_in_securuties_insert AFTER
INSERT ON `investment_in_securities`
FOR EACH ROW BEGIN
INSERT INTO operations_history (`write-off_account`, OPERATION, date_time, SUM, id_of_operation) value(new.`write-off_account`, 'investment_in_securities', now(), new.sum, new.investment_id); END;

// //
DROP TRIGGER IF EXISTS insurance_insert;


CREATE TRIGGER insurance_insert AFTER
INSERT ON `insurance`
FOR EACH ROW BEGIN
INSERT INTO operations_history (`write-off_account`, OPERATION, date_time, SUM, id_of_operation) value(new.`write-off_account`, 'investment_in_insurance', now(), new.sum, new.insurance_id); END;

// //
DROP TRIGGER IF EXISTS investment_in_deposits_update;


CREATE TRIGGER investment_in_deposits_update AFTER
UPDATE ON `investment_in_deposits`
FOR EACH ROW BEGIN
INSERT INTO operations_history (`write-off_account`, OPERATION, date_time, SUM, id_of_operation) value(new.`write-off_account`, 'investment_in_deposits_update', now(), new.sum, new.investment_id); END;

// //
DROP TRIGGER IF EXISTS investment_in_securities_update;


CREATE TRIGGER investment_in_securities_update AFTER
UPDATE ON `investment_in_securities`
FOR EACH ROW BEGIN
INSERT INTO operations_history (`write-off_account`, OPERATION, date_time, SUM, id_of_operation) value(new.`write-off_account`, 'investment_in_securities_update', now(), new.sum, new.investment_id); END;

// //
DROP TRIGGER IF EXISTS insurance_update;


CREATE TRIGGER insurance_update AFTER
UPDATE ON `insurance`
FOR EACH ROW BEGIN
INSERT INTO operations_history (`write-off_account`, OPERATION, date_time, SUM, id_of_operation) value(new.`write-off_account`, 'investment_in_insurance_update', now(), new.sum, new.insurance_id); END;

// //
DROP TRIGGER IF EXISTS investment_in_deposits_delete;


CREATE TRIGGER investment_in_deposits_delete AFTER
DELETE ON `investment_in_deposits`
FOR EACH ROW BEGIN
INSERT INTO operations_history (`write-off_account`, OPERATION, date_time, SUM, id_of_operation) value(old.`write-off_account`, 'investment_in_deposits_delete', now(), old.sum, old.investment_id); END;

// //
DROP TRIGGER IF EXISTS investment_in_securities_delete;


CREATE TRIGGER investment_in_securities_delete AFTER
DELETE ON `investment_in_securities`
FOR EACH ROW BEGIN
INSERT INTO operations_history (`write-off_account`, OPERATION, date_time, SUM, id_of_operation) value(old.`write-off_account`, 'investment_in_securities_delete', now(), old.sum, old.investment_id); END;

// //
DROP TRIGGER IF EXISTS insurance_delete;


CREATE TRIGGER insurance_delete AFTER
DELETE ON `insurance`
FOR EACH ROW BEGIN
INSERT INTO operations_history (`write-off_account`, OPERATION, date_time, SUM, id_of_operation) value(old.`write-off_account`, 'investment_in_insurance_delete', now(), old.sum, old.insurance_id); END;

// -- End of The Third Trigger --
 -- Begin of The Forth Trigger --
//
DROP TRIGGER IF EXISTS clients_delete;


CREATE TRIGGER clients_delete
BEFORE
DELETE ON `clients`
FOR EACH ROW BEGIN -- Check for deposits --
 DECLARE n int DEFAULT 0; DECLARE i int DEFAULT 0; DECLARE id int DEFAULT 0;
SELECT count(client_id)
FROM `client->deposit`
WHERE old.client_id = client_id INTO n;
SET i = 0; WHILE i<n DO
SELECT investment_id
FROM `client->deposit`
WHERE old.client_id = client_id
LIMIT i,
      1 INTO id; IF (
                       (SELECT operation_id
                        FROM operations_history
                        WHERE (OPERATION = 'investment_in_deposits_delete'
                               AND id_of_operation = id)) IS NULL) THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Клиент не забрал свой депозит.'; END IF;
DELETE
FROM `client->deposit`
WHERE investment_id = id;
  DELETE
  FROM `investment->deposit` WHERE investment_id = id;
  DELETE
  FROM investment_in_deposits WHERE investment_id = id;
  SET i = i + 1; END WHILE; -- Check for sequritites --

  SET n = 0;
  SET i =0;
  SET id = 0;
  SELECT count(client_id)
  FROM `client->security` WHERE old.client_id = client_id INTO n; WHILE i<n DO
SELECT investment_id
FROM `client->security`
WHERE old.client_id = client_id
LIMIT i,
      1 INTO id; IF (
                       (SELECT operation_id
                        FROM operations_history
                        WHERE (OPERATION = 'investment_in_securities_delete'
                               AND id_of_operation = id)) IS NULL) THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Клиент не забрал свои ценные бумаги.'; END IF;
DELETE
FROM `client->security`
WHERE investment_id = id;
  DELETE
  FROM `investment->security` WHERE investment_id = id;
  DELETE
  FROM investment_in_securities WHERE investment_id = id;
  SET i = i + 1; END WHILE; -- Check for insurance --

  SET n = 0;
  SET i =0;
  SET id = 0;
  SELECT count(client_id)
  FROM `client->insurance` WHERE old.client_id = client_id INTO n; WHILE i<n DO
SELECT insurance_id
FROM `client->insurance`
WHERE old.client_id = client_id
LIMIT i,
      1 INTO id; IF (
                       (SELECT operation_id
                        FROM operations_history
                        WHERE (OPERATION = 'insurance_delete'
                               AND id_of_operation = id)) IS NULL) THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Клиент не забрал деньги со страхования.'; END IF;
DELETE
FROM `client->insurance`
WHERE insurance_id = id;
  DELETE
  FROM insurance WHERE insurance_id = id;
  SET i = i + 1; END WHILE; END;

// -- End of the Forth trigger --
 -- Start of the Fifth trigger --
//
DROP TRIGGER IF EXISTS deposits_auto_insert;


CREATE TRIGGER deposits_auto_insert
BEFORE
INSERT ON `deposits`
FOR EACH ROW BEGIN
SET new.description = concat('Депозит: ', new.title, ', id: ', cast(new.deposit_id AS char(64)), ', ставка: ', cast(new.rate AS char(64)), ', макс. срок вклада: ', cast(new.max_date AS char(64))); END;

// //
DROP TRIGGER IF EXISTS securities_auto_insert;


CREATE TRIGGER securities_auto_insert
BEFORE
INSERT ON `securities`
FOR EACH ROW BEGIN
SET new.description = concat('Ценная бумага: ', new.title, ', id: ', cast(new.securities_id AS char(64)), ', профит: ', cast(new.profit AS char(64)), ', мин. сумма вклада: ', cast(new.min_sum AS char(64))); END;

// -- End of Fifth trigger --
 -- Start of Sixth trigger --
//
DROP TRIGGER IF EXISTS deposits_min_sum;


CREATE TRIGGER deposits_min_sum
BEFORE
INSERT ON investment_in_deposits
FOR EACH ROW BEGIN DECLARE MIN decimal;
SET MIN =
  (SELECT min_sum
   FROM deposits
   WHERE deposit_id = new.deposit_id); IF (new.sum < MIN) THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Сумма вложения меньше минимальной суммы сложения в данный депозит.'; END IF; END;

// -- End of Sixth trigger --
 -- Start of Seventh trigger --
//
DROP TRIGGER IF EXISTS finish_date_deposits;


CREATE TRIGGER finish_date_deposits
BEFORE
INSERT ON investment_in_deposits
FOR EACH ROW BEGIN DECLARE period_dt int; DECLARE finish_date datetime;
SET period_dt = convert(substring_index(new.period, ' ', 1), unsigned integer);
SET finish_date = date_add(new.date_of_start, INTERVAL period_dt DAY);
SET new.date_of_finish = finish_date; END;

// -- END --