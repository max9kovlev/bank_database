use bank2;

INSERT INTO clients (surname, name, patronymic, phone, address) VALUES 
('Василин', 'Василий', 'Васильевич', '86543217489', 'ул. Пушкина, д.18'),
('Василин', 'Петр', 'Васильевич', '86544567489', 'ул. Пушкина, д.13'),
('Киселев', 'Александр', 'Юрьевич', '84321249087', 'ул. Кирова, д. 82'),
('Виталин', 'Георгий', 'Петрович', '89403325432', 'ул. Октябрьская, д. 28'),
('Александров', 'Петр', 'Генадьевич', '890224567890', 'ул. Первая, д.12'); 

INSERT INTO accounts(type, balance) VALUES
('Дебетовый', 50000),
('Кредитный', 125000),
('Дебетовый', 70000),
('Кредитный', 12000),
('Кредитный', 5000),
('Дебетовый', 27000);

INSERT INTO workers (surname, name, patronymic, education, phone, address, passport_id) VALUES
('Василин', 'Владислав', 'Васильевич', 'Высшее', '865434317489', 'ул. Петрова, д.18', '212441'),
('Кириллин', 'Иван', 'Витальевич', 'Среднее', '86543217489', 'ул. Иванова, д.18', '3213123'),
('Киселев', 'Андрей', 'Юрьевич', 'Высшее' ,'84321859087', 'ул. Соболева, д. 82', '21312'),
('Фомин', 'Георгий', 'Петрович', 'Высшее', '894033237832', 'ул. Октябрьская, д. 28', '21312'),
('Александров', 'Петр', 'Генадьевич', 'Высшее', '890298767890', 'ул. Вторая, д.12', '21421'),
('Зубарев', 'Петр', 'Витальевич', 'Высшее', '84325678908','ул. Колотушкина, д.12', '21979');

INSERT INTO bank (title, legal_address) VALUE ('Росбанк', 'ул. Советская, д. 15А');
SET SQL_SAFE_UPDATES = 0;

INSERT INTO branches (phone, address) VALUES
('86-44-33', 'ул. Третья, д. 88б'),
('77-33-22', 'ул. Четвертая, д. 38г');

INSERT INTO deposits (rate, title, min_sum, max_period) VALUES
(12, 'Нужный', 10000, '0001-0-0'),
(11, 'Надежный', 12000, '0010-0-0'),
(13, 'Особенный', 10000, '0012-0-0'),
(19, 'Молодежный', 32000, '0003-0-0'),
(12, 'Классический', 64000, '0032-0-0');

INSERT INTO investment_in_deposits (sum, date, period) VALUES
(10000, now(), '0002-00-00'),
(15000, now(), '0013-00-00'),
(33000, now(), '0004-00-00');

INSERT INTO insurance (type, min_sum) VALUES
('Имущество', 100000),
('Недвижимость', 200000),
('Движимое имущество', 50000),
('Машина', 30000),
('Бизнес', 75000);

INSERT INTO investment_in_insurance (sum, date, period) VALUES
(120000, now(), '0002-00-00'),
(110000, now(), '0013-00-00'),
(360000, now(), '0004-00-00');

INSERT INTO securities (title, min_sum, profit) VALUES
('GOOGLE', 500000, 12),
('Sberbank', 10000, 7),
('Amazon', 880000, 15),
('AliExpress', 123000, 19),
('AlfaStroi', 25000, 12);

INSERT INTO investment_in_securities (sum, date, period) VALUES
(120000, now(), '0003-00-00'),
(110000, now(), '0012-00-00'),
(360000, now(), '0007-00-00');

INSERT INTO posts (title, salary) VALUES
('Программист', 100000),
('Бухгалтер', 90000),
('Сисадмин', 70000),
('Инженер', 80000),
('Консультант', 65000);

INSERT INTO quotes (date_time, quote) VALUES
(now(), 126),
(now(),432),
(now(),222),
(now(),555),
(now(),876);

INSERT INTO operations_history (operation, date_time, sum) VALUES 
('investment_in_deposits', now(), 10000),
('investment_in_deposits', now(), 15000),
('account', now(), 50000),
('account', now(), 125000);

INSERT INTO `client->operation` VALUES
(1,1),
(1,2),
(1,3),
(1,4);

INSERT INTO `bank->branch` VALUES
(1,1),
(1,2);

INSERT INTO `client->account` VALUES
(1,1),
(1,2),
(2,3),
(3,4),
(4,5),
(5,6);

INSERT INTO `client->deposit` VALUES
(1,1),
(1,2),
(2,3);

INSERT INTO `client->insurance` VALUES
(2,1),
(3,2),
(5,3);

INSERT INTO `client->security` VALUES
(2,1),
(3,2),
(4,3);

INSERT INTO `investment->deposit` VALUES
(1,1),
(2,1),
(3,2);

INSERT INTO `investment->security`VALUES
(1,2),
(2,1),
(3,3);

INSERT INTO `investment->insurance`VALUES
(1,2),
(2,1),
(3,3);


INSERT INTO `worker->branch` VALUES
(1,1),
(2,1),
(3,1),
(4,2),
(5,2),
(6,2);

INSERT INTO `worker->post` VALUES
(1,1),
(2,2),
(3,5),
(4,1),
(5,2),
(6,5);




