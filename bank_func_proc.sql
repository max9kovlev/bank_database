DELIMITER // -- drop function if exists MoneyCount;

CREATE FUNCTION MoneyCount(surname_ varchar(64), name_ varchar(64), patronymic_ varchar(64)) RETURNS decimal READS SQL DATA BEGIN DECLARE client_id_ int;
SET client_id_ =
  (SELECT client_id
   FROM clients
   WHERE surname = surname_
     AND name = name_
     AND patronymic = patronymic_); -- investment_in_deposits --

SET @count = IFNULL(
                      (SELECT sum(sum_)
                       FROM
                         (SELECT in_dep.sum AS sum_
                          FROM investment_in_deposits AS in_dep
                          JOIN `client->deposit` AS c2d ON c2d.client_id = client_id_
                          JOIN `client->operation` AS c2o ON c2d.client_id = client_id_
                          JOIN operations_history AS oh ON oh.operation_id = c2o.operation_id
                          AND OPERATION = 'investment_in_deposits'
                          WHERE in_dep.investment_id = c2d.investment_id
                            AND in_dep.sum IS NOT NULL
                          GROUP BY in_dep.investment_id
                          HAVING sum(in_dep.sum) > 0) AS T),0); -- investment_in_securities --

SET @count = @count + IFNULL(
                               (SELECT sum(sum_)
                                FROM
                                  (SELECT in_sec.sum AS sum_
                                   FROM investment_in_securities AS in_sec
                                   JOIN `client->security` AS c2s ON c2s.client_id = client_id_
                                   JOIN `client->operation` AS c2o ON c2o.client_id = client_id_
                                   JOIN operations_history AS oh ON oh.operation_id = c2o.operation_id
                                   AND OPERATION = 'investment_in_securities'
                                   WHERE in_sec.investment_id = c2s.investment_id
                                     AND in_sec.sum IS NOT NULL
                                   GROUP BY in_sec.investment_id
                                   HAVING sum(in_sec.sum) > 0) AS T),0); -- investment_in_insurance --

SET @count = @count + IFNULL(
                               (SELECT sum(sum_)
                                FROM
                                  (SELECT in_ins.sum AS sum_
                                   FROM investment_in_insurance AS in_ins
                                   JOIN `client->insurance` AS c2i ON c2i.client_id = client_id_
                                   JOIN `client->operation` AS c2o ON c2o.client_id = client_id_
                                   JOIN operations_history AS oh ON oh.operation_id = c2o.operation_id
                                   AND OPERATION = 'investment_in_insurance'
                                   WHERE in_ins.investment_id = c2i.investment_id
                                     AND in_ins.sum IS NOT NULL
                                   GROUP BY in_ins.investment_id
                                   HAVING sum(in_ins.sum) > 0) AS T),0); -- accounts --

SET @count = @count + IFNULL(
                               (SELECT sum(balance_)
                                FROM
                                  (SELECT balance AS balance_
                                   FROM accounts
                                   JOIN `client->account` AS c2a ON c2a.client_id = client_id_
                                   JOIN `client->operation` AS c2o ON c2o.client_id = client_id_
                                   JOIN operations_history AS oh ON oh.operation_id = c2o.operation_id
                                   AND OPERATION = 'account'
                                   WHERE accounts.bill_id = c2a.bill_id
                                     AND accounts.balance IS NOT NULL
                                   GROUP BY accounts.bill_id
                                   HAVING sum(accounts.balance) > 0) AS T),0);
INSERT INTO info_log (log, date) value (CONCAT('Была запрощена информация о деньгах клиента: ', surname_, ' ', name_, ' ', patronymic_), now()); -- 0.5
 RETURN @count; END;

// // -- drop procedure if exists DeleteClient;

CREATE PROCEDURE DeleteClient(surname_ varchar(64), name_ varchar(64), patronymic_ varchar(64)) MODIFIES SQL DATA BEGIN
SET @client_id_ =
  (SELECT client_id
   FROM clients
   WHERE surname = surname_
     AND name = name_
     AND patronymic = patronymic_); IF ((MoneyCount(surname_, name_, patronymic_)) != 0) THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Клиент забрал не все деньги из банка.'; END IF;
DELETE
FROM accounts
WHERE bill_id in
    (SELECT bill_id
     FROM `client->account`
     WHERE client_id = @client_id_);
  DELETE
  FROM investment_in_deposits WHERE investment_id in
    (SELECT investment_id
     FROM `client->deposit`
     WHERE client_id = @client_id_);
  DELETE
  FROM investment_in_securities WHERE investment_id in
    (SELECT investment_id
     FROM `client->security`
     WHERE client_id = @client_id_);
  DELETE
  FROM investment_in_insurance WHERE investment_id in
    (SELECT investment_id
     FROM `client->insurance`
     WHERE client_id = @client_id_);
  DELETE
  FROM `client->account` WHERE client_id = @client_id_;
  DELETE
  FROM `client->deposit` WHERE client_id = @client_id_;
  DELETE
  FROM `client->insurance` WHERE client_id = @client_id_;
  DELETE
  FROM `client->security` WHERE client_id = @client_id_;
  DELETE
  FROM clients WHERE client_id = @client_id_;
  INSERT INTO info_log (log, date) value (CONCAT('Был удален клиент: ', surname_, ' ', name_, ' ', patronymic_), now()); END;

// // -- drop function if exists GetWorkertInf;

CREATE FUNCTION GetWorkerInf(surname_ varchar(64), name_ varchar(64), patronymic_ varchar(64)) RETURNS varchar(512) READS SQL DATA BEGIN DECLARE RESULT varchar(512);
SET RESULT =
  (SELECT concat('Работник id : ', W.worker_id, ', ФИО: ', W.surname, ' ', W.name, ' ', W.patronymic, ', образование: ', W.education, ', должность: ', posts.title, ', зарплата: ', posts.salary, ', отделение id: ', branches.branch_id, ', адрес отделения: ', branches.address)
   FROM workers AS W
   JOIN `worker->post` AS w2p ON W.worker_id = w2p.post_id
   JOIN `worker->branch` AS w2b ON W.worker_id = w2b.branch_id
   JOIN posts ON w2p.post_id = posts.post_id
   JOIN branches ON branches.branch_id = w2b.branch_id
   WHERE W.surname = surname_
     AND W.name = name_
     AND W.patronymic = patronymic_
   GROUP BY W.worker_id); IF (RESULT IS NULL) THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Такого работника не существует.'; END IF;
INSERT INTO info_log (log, date) value (CONCAT('Была запрошена информация о работнике: ', surname_, ' ', name_, ' ', patronymic_), now()); RETURN RESULT; END;

// // -- drop procedure if exists InvestStats;

CREATE PROCEDURE InvestStats() MODIFIES SQL DATA BEGIN
DROP TABLE IF EXISTS dep_stats;
CREATE
TEMPORARY TABLE dep_stats (ID int, Title varchar(64),
                                         Inv_count int DEFAULT 0,
                                                               Total_sum decimal DEFAULT 0,
                                                                                         Client_count int DEFAULT 0);
INSERT INTO dep_stats
  (SELECT D.deposit_id,
          D.title,
          count(id.investment_id),
          sum(id.sum),
          count(distinct(c2d.client_id))
   FROM deposits AS D
   JOIN `investment->deposit` AS i2d ON i2d.deposit_id = D.deposit_id
   JOIN investment_in_deposits AS id ON i2d.investment_id = id.investment_id
   JOIN `client->deposit` AS c2d ON id.investment_id = c2d.investment_id
   GROUP BY D.deposit_id
   HAVING count(id.investment_id) !=0);
INSERT INTO info_log (log, date) value ('Была запрошена информация о статистике по депозитам.',
                                        now());
DROP TABLE IF EXISTS sec_stats;
CREATE
TEMPORARY TABLE sec_stats (ID int, Title varchar(64),
                                         Inv_count int DEFAULT 0,
                                                               Total_sum decimal DEFAULT 0,
                                                                                         Client_count int DEFAULT 0);
INSERT INTO sec_stats -- 0.5

  (SELECT S.securities_id,
          S.title,
          count(ins.investment_id),
          sum(ins.sum),
          count(distinct(c2s.client_id))
   FROM securities AS S
   JOIN `investment->security` AS i2s ON i2s.securities_id = S.securities_id
   JOIN investment_in_securities AS ins ON ins.investment_id = i2s.investment_id
   JOIN `client->security` AS c2s ON ins.investment_id = c2s.investment_id -- 0.6

   GROUP BY S.securities_id
   HAVING count(ins.investment_id) !=0);
INSERT INTO info_log (log, date) value ('Была запрошена информация о статистике по ценным бумагам.',
                                        now());
DROP TABLE IF EXISTS ins_stats;
CREATE
TEMPORARY TABLE ins_stats (ID int, Title varchar(64),
                                         Inv_count int DEFAULT 0,
                                                               Total_sum decimal DEFAULT 0,
                                                                                         Client_count int DEFAULT 0);
INSERT INTO ins_stats -- 0.5

  (SELECT I.insurance_id,
          I.type,
          count(ii.investment_id),
          sum(ii.sum),
          count(distinct(c2i.client_id))
   FROM insurance AS I
   JOIN `investment->insurance` AS i2i ON i2i.insurance_id = I.insurance_id
   JOIN investment_in_insurance AS ii ON ii.investment_id = i2i.investment_id
   JOIN `client->insurance` AS c2i ON ii.investment_id = c2i.investment_id
   GROUP BY I.insurance_id
   HAVING count(ii.investment_id) !=0);
INSERT INTO info_log (log, date) value ('Была запрошена информация о статистике по страхованию.',
                                        now());
SELECT *
FROM ins_stats;
SELECT *
FROM dep_stats;
SELECT *
FROM sec_stats; END; // // -- drop function if exists ClientInfo;

CREATE FUNCTION ClientInfo(surname_ varchar(64), name_ varchar(64), patronymic_ varchar(64)) RETURNS varchar(512) READS SQL DATA BEGIN DECLARE RESULT varchar(512);
SET RESULT = concat('Кол-во денег: ', MoneyCount(surname_, name_, patronymic_), ', ',
                      (SELECT concat('кол-во счетов: ', count(distinct(c2a.bill_id)), ', кол-во депозитов: ', count(distinct(c2d.investment_id)), ', кол-во страхований: ', count(distinct(c2i.investment_id)), ', кол-во ценных бумаг: ', count(distinct(c2s.investment_id)))
                       FROM clients AS C
                       LEFT JOIN `client->account` AS c2a ON C.client_id = c2a.client_id
                       LEFT JOIN `client->deposit` AS c2d ON C.client_id = c2d.client_id
                       LEFT JOIN `client->insurance` AS c2i ON C.client_id = c2i.client_id
                       LEFT JOIN`client->security` AS c2s ON C.client_id = c2a.client_id
                       WHERE C.surname = surname_
                         AND C.name = name_
                         AND C.patronymic = patronymic_
                       GROUP BY C.client_id));
INSERT INTO info_log (log, date) value (CONCAT('Была запрощена информация о статистике клиента: ', surname_, ' ', name_, ' ', patronymic_), now()); RETURN RESULT; END; // // -- drop procedure if exists ShowWorkersBracnhes;

CREATE PROCEDURE ShowWorkersBranches() READS SQL DATA BEGIN DECLARE i int DEFAULT 1; DECLARE n int DEFAULT 0;
DROP TABLE IF EXISTS temp_Workers;
CREATE
TEMPORARY TABLE temp_Workers (worker_id int, surname varchar(64),
                                                     name varchar(64),
                                                          patronymic varchar(64),
                                                                     post varchar(64),
                                                                          salary decimal);
INSERT INTO temp_Workers
  (SELECT W.worker_id,
          W.surname,
          W.name,
          W.patronymic,
          posts.title,
          posts.salary
   FROM workers AS W
   JOIN `worker->branch` AS w2b ON w2b.worker_id = W.worker_id
   JOIN `worker->post` AS w2p ON w2p.worker_id = W.worker_id
   JOIN posts ON posts.post_id = w2p.post_id
   GROUP BY W.worker_id);
SELECT count(branches.branch_id)
FROM branches INTO n; WHILE i<=n DO
SELECT *
FROM temp_Workers
WHERE worker_id in
    (SELECT worker_id
     FROM `worker->branch` AS w2b
     WHERE w2b.branch_id = i) ;
  SET i = i+1; END WHILE;
  INSERT INTO info_log (log, date) value ('Была запрошена информация о работниках по отделениям.',
                                          now()); END; // // -- drop procedure if exists InvestMoney;

CREATE PROCEDURE InvestMoney(surname_ varchar(64), name_ varchar(64), patronymic_ varchar(64), sum_ decimal, period datetime, title_ varchar(64), type_ varchar(64)) MODIFIES SQL DATA BEGIN DECLARE client_id_ int;
SELECT client_id
FROM clients
WHERE surname = surname_
  AND name = name_
  AND patronymic = patronymic_ INTO client_id_; IF (type_ = 'Депозиты') THEN IF (
                                                                                   (SELECT min_sum
                                                                                    FROM deposits
                                                                                    WHERE title=title_
                                                                                      AND min_sum <= sum_) IS NULL) THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Инвестируемая сумма меньше минимально допустимой.'; END IF;
INSERT INTO investment_in_deposits (SUM, date, period) value(sum_, now(), period);
SET @inv_id = last_insert_id();
INSERT INTO `client->deposit` value(client_id_, @inv_id);
INSERT INTO `investment->deposit` value(@inv_id,
                                          (SELECT deposit_id
                                           FROM deposits
                                           WHERE title=title_));
INSERT INTO operations_history(OPERATION, date_time, SUM) value('investment_in_deposit', now(), sum_);
INSERT INTO `client->operation`value(client_id_, last_insert_id()); END IF; IF (type_ = 'Ценные бумаги') THEN IF (
                                                                                                                    (SELECT min_sum
                                                                                                                     FROM securities
                                                                                                                     WHERE title=title_
                                                                                                                       AND min_sum <= sum_) IS NULL) THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Инвестируемая сумма меньше минимально допустимой.'; END IF;
INSERT INTO investment_in_securities (SUM, date, period) value(sum_, now(), period);
SET @inv_id = last_insert_id();
INSERT INTO `client->security` value(client_id_, @inv_id);
INSERT INTO `investment->security` value(@inv_id,
                                           (SELECT securities_id
                                            FROM securities
                                            WHERE title=title_));
INSERT INTO operations_history(OPERATION, date_time, SUM) value('investment_in_sequrities', now(), sum_);
INSERT INTO `client->operation`value(client_id_, last_insert_id()); END IF; END; // // -- drop procedure if exists BankSummary;

CREATE PROCEDURE BankSummary() MODIFIES SQL DATA BEGIN
SELECT B.title,
       B.legal_address,
       count(distinct(BR.branch_id)) AS 'Кол-во отделений',
       count(distinct(A.bill_id)) AS 'Кол-во счетов',
       count(distinct(ID.investment_id)) AS 'Кол-во инвест. в депозиты',
       count(distinct(INS.investment_id)) AS 'Кол-во инвест. в страхования'
FROM bank AS B
JOIN `bank->branch` AS b2b ON b2b.bank_id = B.bank_id
JOIN branches AS BR ON b2b.branch_id = BR.branch_id
JOIN accounts AS A
JOIN investment_in_deposits AS ID
JOIN investment_in_securities AS INS
WHERE B.bank_id = 1
GROUP BY B.bank_id;
INSERT INTO info_log (log, date) value ('Была запрощена информация о банке.', now()); END; // // -- drop procedure if exists ChangePosition;

CREATE PROCEDURE ChangePosition(surname_ varchar(64), name_ varchar(64), patronymic_ varchar(64), POSITION varchar(64), salary_ decimal) READS SQL DATA BEGIN DECLARE worker_id_ int;
SELECT worker_id
FROM workers
WHERE surname = surname_
  AND name = name_
  AND patronymic = patronymic_ INTO worker_id_;
UPDATE `worker->post`
SET post_id =
  (SELECT post_id
   FROM posts
   WHERE title = POSITION
     AND salary = salary_);
INSERT INTO info_log (log,date) value (concat('Была изменена должность у работника id: ', worker_id_, ' на ', 'position'), now()); END; -- total: 1.4
//