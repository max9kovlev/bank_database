 -- First trigger --

SELECT *
FROM bank;


SELECT *
FROM clients;


SELECT *
FROM workers;


INSERT INTO bank value(1, 'Сбербанк', 'адрес', 0, 0);


INSERT INTO clients
VALUES(1,'Петров','Александр','Викторович', '3243244', 'adress'),
      (2,'Сидоров','Александр','Викторович', '32435674', 'adress2'),
      (3,'Александров','Александр','Петрович', '32223244', 'adress3'),
      (4,'Викторов','Петр','Викторович', '33356643244', 'adress5'),
      (5,'Зубенко','Александр','Петрович', '313373244', 'adres6');


INSERT INTO workers
VALUES(1,'Жмышенко','Валерий','Альбертович', 'high', '1334'),
      (2,'Петренко','Алекс','Викторович', 'high', '5552'),
      (3,'Шахмузаев','Заур','Хабибович', 'high', '435'),
      (4,'Кадыров', 'Ахмат', 'Абдулович', 'low', '34578'),
      (5, 'Навальный', 'Алексей', 'Анатольевич','high', '1337');


SELECT *
FROM bank;


SELECT *
FROM clients;


SELECT *
FROM workers;


DELETE
FROM clients
WHERE client_id = 4;


DELETE
FROM workers
WHERE worker_id = 4;


SELECT *
FROM bank;


SELECT *
FROM clients;


SELECT *
FROM workers;

-- End of First Trigger --
 -- Second Trigger  --

INSERT INTO operations_history value(1, 1, 'operation', '2017-10-10', '1000', 1);


INSERT INTO operations_history value(2, 1, 'operation', '2017-09-10', '1000', 1);


INSERT INTO quotes value(1, '2017-10-10', 3232);


INSERT INTO quotes value(2, '2017-09-10', 222);


INSERT INTO deposits (deposit_id, rate, title, max_date, min_sum) value(1, 13, 'Пенсионный', 365, 1000);


INSERT INTO investment_in_deposits (SUM, date_of_start, date_of_finish, deposit_id, `write-off_account`, period) value(10000, '2000-01-27', '2017-01-27', 1, 1, '17 лет');

-- End of Second Trigger --
 -- Third trigger --

INSERT INTO investment_in_deposits (SUM, date_of_start, deposit_id, `write-off_account`, period) value(10000, '2019-11-10', 1, 1, '180 дней');


SELECT *
FROM operations_history;


UPDATE investment_in_deposits
SET SUM = 133700
WHERE investment_id = 2;


SELECT *
FROM operations_history;


DELETE
FROM investment_in_deposits
WHERE investment_id = 2;


SELECT *
FROM operations_history;

-- End of Third Trigger --
 -- Forth Trigger --

SELECT *
FROM investment_in_deposits;


SELECT *
FROM clients;


INSERT INTO investment_in_deposits (SUM, date_of_start, deposit_id, `write-off_account`, period) value(10000, '2019-11-10', 1, 1, '180 дней');


INSERT INTO investment_in_securities (SUM, securities_id, `write-off_account`, date_time) value(10000, 1, 1, '2019-10-11');


INSERT INTO insurance value(1, 'Личное', 10000, 13, 1, '2019-10-11');


SELECT *
FROM insurance;


INSERT INTO `client->deposit` value(1, 3);


DELETE
FROM clients
WHERE client_id=1;


INSERT INTO `client->insurance` value(2, 1);


DELETE
FROM clients
WHERE client_id=2;


INSERT INTO `client->security` value(3, 1);


DELETE
FROM clients
WHERE client_id = 3;

-- End of Forth Trigger --
 -- Fifth Trigger--

INSERT INTO deposits (deposit_id, rate, title, max_date, min_sum) value(2, 14, 'Молодежный', 700, 10000);


SELECT *
FROM deposits;

-- alter table securities drop extra_information;

INSERT INTO securities (title, min_sum, profit) value('Google', 100000, 120);


SELECT *
FROM securities;

-- End of Fifth trigger --
 -- Sixth trigger --

SELECT *
FROM deposits;


INSERT INTO investment_in_deposits (SUM, date_of_start, deposit_id, `write-off_account`, period) value(1, '2019-11-10', 1, 1, '30 дней');

-- End --
 -- Seventh trigger --

INSERT INTO investment_in_deposits (SUM, date_of_start, deposit_id, `write-off_account`, period) value(10000, '2019-11-10', 1, 1, '300 дней');


SELECT *
FROM investment_in_deposits;
