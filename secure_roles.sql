USE bank2;


DROP ROLE IF EXISTS bank_admin,
                    bank_client,
                    bank_worker;


CREATE ROLE bank_admin,
            bank_client,
            bank_worker;

-- grants for bank_admin --
GRANT ALL ON bank2.* TO bank_admin;

-- grants for bank_client --
GRANT
SELECT ON bank2.deposits TO bank_client;

GRANT
SELECT ON bank2.insurance TO bank_client;

GRANT
SELECT ON bank2.quotes TO bank_client;

GRANT
SELECT ON bank2.securities TO bank_client;

GRANT
SELECT ON bank2.bank TO bank_client;

GRANT
SELECT ON bank2.branches TO bank_client;

-- grants for bank_worker --
GRANT
SELECT,
UPDATE,
INSERT ON bank2.accounts TO bank_worker;

GRANT
SELECT,
UPDATE,
INSERT ON bank2.insurance TO bank_worker;

GRANT
SELECT,
UPDATE,
INSERT ON bank2.`client->account` TO bank_worker;

GRANT
SELECT,
UPDATE,
INSERT ON bank2.`client->deposit` TO bank_worker;

GRANT
SELECT,
UPDATE,
INSERT ON bank2.`client->insurance` TO bank_worker;

GRANT
SELECT,
UPDATE,
INSERT ON bank2.`client->operation` TO bank_worker;

GRANT
SELECT,
UPDATE,
INSERT ON bank2.`client->security` TO bank_worker;

GRANT
SELECT,
UPDATE,
INSERT ON bank2.`investment->deposit` TO bank_worker;

GRANT
SELECT,
UPDATE,
INSERT ON bank2.`investment->security` TO bank_worker;

GRANT
SELECT,
UPDATE,
INSERT ON bank2.`investment->security` TO bank_worker;

GRANT
SELECT,
UPDATE,
INSERT ON bank2.`security->quote` TO bank_worker;

GRANT
SELECT,
UPDATE,
INSERT ON bank2.clients TO bank_worker;

GRANT
SELECT,
UPDATE,
INSERT ON bank2.customer_data_protection TO bank_worker;

GRANT
SELECT,
UPDATE,
INSERT ON bank2.investment_in_deposits TO bank_worker;

GRANT
SELECT,
UPDATE,
INSERT ON bank2.investment_in_securities TO bank_worker;

-- show grants for three roles --
SHOW grants
FOR bank_admin;

SHOW grants
FOR bank_worker;

SHOW grants
FOR bank_client;

-- create users --

DROP USER IF EXISTS admin_user@localhost,
                    worker_user@localhost,
                    client_user@localhost;


CREATE USER admin_user@localhost IDENTIFIED BY 'admin';


CREATE USER worker_user@localhost IDENTIFIED BY 'worker';


CREATE USER client_user@localhost IDENTIFIED BY 'client';

-- set them roles --
GRANT bank_admin TO admin_user@localhost;

GRANT bank_worker TO worker_user@localhost;

GRANT bank_client TO client_user@localhost;


SET DEFAULT ROLE bank_admin TO `admin_user`@`localhost`;
set default role bank_worker to `worker_user`@`localhost`;
set default role bank_client to `client_user`@`localhost`;

FLUSH PRIVILEGES;