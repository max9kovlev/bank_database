CREATE DATABASE IF NOT EXISTS bank2; 

USE bank2; 

DROP TABLE IF EXISTS `bank`; 

CREATE TABLE `bank` 
  ( 
     `bank_id`       INTEGER NOT NULL auto_increment, 
     `title`         VARCHAR(64) NOT NULL, 
     `legal_address` VARCHAR(128) NOT NULL, 
     PRIMARY KEY (`bank_id`) 
  ); 

DROP TABLE IF EXISTS `accounts`; 

CREATE TABLE `accounts` 
  ( 
     `bill_id` INTEGER NOT NULL auto_increment, 
     `type`    VARCHAR(64) NOT NULL, 
     `balance` DECIMAL NOT NULL, 
     PRIMARY KEY(`bill_id`) 
  ); 

DROP TABLE IF EXISTS `clients`; 

CREATE TABLE `clients` 
  ( 
     `client_id`  INTEGER NOT NULL auto_increment, 
     `surname`    VARCHAR(64) NOT NULL, 
     `name`       VARCHAR(64) NOT NULL, 
     `patronymic` VARCHAR(64) NOT NULL, 
     `phone`      VARCHAR(64) NOT NULL, 
     `address`    VARCHAR(64) NOT NULL, 
     PRIMARY KEY(`client_id`) 
  ); 

DROP TABLE IF EXISTS `operations_history`; 

CREATE TABLE `operations_history` 
  ( 
     `operation_id` INTEGER NOT NULL auto_increment, 
     `operation`    VARCHAR(64) NOT NULL, 
     `date_time`    DATETIME NOT NULL, 
     `sum`          DECIMAL NOT NULL, 
     PRIMARY KEY(`operation_id`) 
  ); 

SET sql_safe_updates = 0; 

DROP TABLE IF EXISTS `branches`; 

CREATE TABLE `branches` 
  ( 
     `branch_id` INTEGER NOT NULL auto_increment, 
     `phone`     VARCHAR(64) NOT NULL, 
     `address`   VARCHAR(64) NOT NULL, 
     PRIMARY KEY (`branch_id`) 
  ); 

DROP TABLE IF EXISTS `workers`; 

CREATE TABLE `workers` 
  ( 
     `worker_id`   INTEGER NOT NULL auto_increment, 
     `surname`     VARCHAR(64) NOT NULL, 
     `name`        VARCHAR(64) NOT NULL, 
     `patronymic`  VARCHAR(64) NOT NULL, 
     `phone`       VARCHAR(64) NOT NULL, 
     `address`     VARCHAR(64) NOT NULL, 
     `education`   VARCHAR(64) NOT NULL, 
     `passport_id` VARCHAR(64) NOT NULL, 
     PRIMARY KEY(`worker_id`) 
  ); 

DROP TABLE IF EXISTS `posts`; 

CREATE TABLE `posts` 
  ( 
     `post_id` INTEGER NOT NULL auto_increment, 
     `title`   VARCHAR(64) NOT NULL, 
     `salary`  DECIMAL NOT NULL, 
     PRIMARY KEY (`post_id`) 
  ); 

DROP TABLE IF EXISTS `insurance`; 

CREATE TABLE `insurance` 
  ( 
     `insurance_id` INTEGER NOT NULL auto_increment, 
     `type`         VARCHAR(64) NOT NULL, 
     `min_sum`      DECIMAL NOT NULL, 
     PRIMARY KEY (`insurance_id`) 
  ); 

DROP TABLE IF EXISTS `investment_in_insurance`; 

CREATE TABLE `investment_in_insurance` 
  ( 
     `investment_id` INTEGER NOT NULL auto_increment, 
     `sum`           DECIMAL NOT NULL, 
     `date`          DATETIME, 
     `period`        DATETIME, 
     PRIMARY KEY (`investment_id`) 
  ); 

DROP TABLE IF EXISTS `investment_in_securities`; 

CREATE TABLE `investment_in_securities` 
  ( 
     `investment_id` INTEGER NOT NULL auto_increment, 
     `sum`           DECIMAL NOT NULL, 
     `date`          DATETIME, 
     `period`        DATETIME, 
     PRIMARY KEY (`investment_id`) 
  ); 

DROP TABLE IF EXISTS `securities`; 

CREATE TABLE `securities` 
  ( 
     `securities_id` INTEGER NOT NULL auto_increment, 
     `title`         VARCHAR(64) NOT NULL, 
     `min_sum`       DECIMAL NOT NULL, 
     `profit`        DECIMAL NOT NULL, 
     PRIMARY KEY (`securities_id`) 
  ); 

SET sql_safe_updates = 0; 

DROP TABLE IF EXISTS `deposits`; 

CREATE TABLE `deposits` 
  ( 
     `deposit_id` INTEGER NOT NULL auto_increment, 
     `rate`       DECIMAL NOT NULL, 
     `title`      VARCHAR(64) NOT NULL, 
     `min_sum`    DECIMAL, 
     `max_period` DATETIME, 
     PRIMARY KEY (`deposit_id`) 
  ); 

DROP TABLE IF EXISTS `quotes`; 

CREATE TABLE `quotes` 
  ( 
     `quote_id`  INTEGER NOT NULL auto_increment, 
     `date_time` DATETIME NOT NULL, 
     `quote`     DECIMAL NOT NULL, 
     PRIMARY KEY (`quote_id`) 
  ); 

DROP TABLE IF EXISTS `worker->post`; 

CREATE TABLE `worker->post` 
  ( 
     `worker_id` INTEGER NOT NULL, 
     `post_id`   INTEGER NOT NULL 
  ); 

DROP TABLE IF EXISTS `client->account`; 

CREATE TABLE `client->account` 
  ( 
     `client_id` INTEGER NOT NULL, 
     `bill_id`   INTEGER NOT NULL 
  ); 

DROP TABLE IF EXISTS `worker->branch`; 

CREATE TABLE `worker->branch` 
  ( 
     `worker_id` INTEGER NOT NULL, 
     `branch_id` INTEGER NOT NULL 
  ); 

DROP TABLE IF EXISTS `bank->branch`; 

CREATE TABLE `bank->branch` 
  ( 
     `bank_id`   INTEGER NOT NULL, 
     `branch_id` INTEGER NOT NULL 
  ); 

DROP TABLE IF EXISTS `client->operation`; 

CREATE TABLE `client->operation` 
  ( 
     `client_id`    INTEGER NOT NULL, 
     `operation_id` INTEGER NOT NULL 
  ); 

DROP TABLE IF EXISTS `client->insurance`; 

CREATE TABLE `client->insurance` 
  ( 
     `client_id`     INTEGER NOT NULL, 
     `investment_id` INTEGER NOT NULL 
  ); 

DROP TABLE IF EXISTS `client->deposit`; 

CREATE TABLE `client->deposit` 
  ( 
     `client_id`     INTEGER NOT NULL, 
     `investment_id` INTEGER NOT NULL 
  ); 

DROP TABLE IF EXISTS `client->security`; 

CREATE TABLE `client->security` 
  ( 
     `client_id`     INTEGER NOT NULL, 
     `investment_id` INTEGER NOT NULL 
  ); 

DROP TABLE IF EXISTS `investment_in_deposits`; 

CREATE TABLE `investment_in_deposits` 
  ( 
     `investment_id` INTEGER NOT NULL auto_increment, 
     `sum`           DECIMAL NOT NULL, 
     `date`          DATETIME, 
     `period`        DATETIME, 
     PRIMARY KEY (`investment_id`) 
  ); 

DROP TABLE IF EXISTS `investment->deposit`; 

CREATE TABLE `investment->deposit` 
  ( 
     `investment_id` INTEGER NOT NULL, 
     `deposit_id`    INTEGER NOT NULL 
  ); 

DROP TABLE IF EXISTS `investment->security`; 

CREATE TABLE `investment->security` 
  ( 
     `investment_id` INTEGER NOT NULL, 
     `securities_id` INTEGER NOT NULL 
  ); 

DROP TABLE IF EXISTS `security->quote`; 

CREATE TABLE `security->quote` 
  ( 
     `securities_id` INTEGER NOT NULL, 
     `quote_id`      INTEGER NOT NULL 
  ); 

DROP TABLE IF EXISTS `info_log`; 

CREATE TABLE `info_log` 
  ( 
     log  VARCHAR(512), 
     date DATETIME 
  ); 

DROP TABLE IF EXISTS `investment->insurance`; 

CREATE TABLE `investment->insurance` 
  ( 
     investment_id INT, 
     insurance_id  INT 
  ); 
  
ALTER TABLE `client->account` 
  ADD FOREIGN KEY (client_id) REFERENCES `clients` (`client_id`), 
  ADD FOREIGN KEY (bill_id) REFERENCES `accounts` (`bill_id`); 

ALTER TABLE `worker->branch` 
  ADD FOREIGN KEY (worker_id) REFERENCES `workers` (`worker_id`), 
  ADD FOREIGN KEY (branch_id) REFERENCES `branches` (`branch_id`); 

ALTER TABLE `worker->post` 
  ADD FOREIGN KEY (worker_id) REFERENCES `workers` (`worker_id`), 
  ADD FOREIGN KEY (post_id) REFERENCES `posts` (`post_id`); 

ALTER TABLE `bank->branch` 
  ADD FOREIGN KEY (bank_id) REFERENCES `bank` (`bank_id`), 
  ADD FOREIGN KEY (branch_id) REFERENCES `branches` (`branch_id`); 

ALTER TABLE `client->operation` 
  ADD FOREIGN KEY (client_id) REFERENCES `clients` (`client_id`), 
  ADD FOREIGN KEY (operation_id) REFERENCES `operations_history` (`operation_id` 
  ); 

ALTER TABLE `client->insurance` 
  ADD FOREIGN KEY (client_id) REFERENCES `clients` (`client_id`), 
  ADD FOREIGN KEY (insurance_id) REFERENCES `insurance` (`insurance_id`); 

ALTER TABLE `client->deposit` 
  ADD FOREIGN KEY (client_id) REFERENCES `clients` (`client_id`), 
  ADD FOREIGN KEY (investment_id) REFERENCES `investment_in_deposits` ( 
  `investment_id`); 

ALTER TABLE `client->security` 
  ADD FOREIGN KEY (client_id) REFERENCES `clients` (`client_id`), 
  ADD FOREIGN KEY (investment_id) REFERENCES `investment_in_securities` ( 
  `investment_id`); 

ALTER TABLE `investment->deposit` 
  ADD FOREIGN KEY (investment_id) REFERENCES `investment_in_deposits` ( 
  `investment_id`), 
  ADD FOREIGN KEY (deposit_id) REFERENCES `deposits` (`deposit_id`); 

ALTER TABLE `investment->security` 
  ADD FOREIGN KEY (investment_id) REFERENCES `investment_in_securities` ( 
  `investment_id`), 
  ADD FOREIGN KEY (securities_id) REFERENCES `securities` (`securities_id`); 

ALTER TABLE `security->quote` 
  ADD FOREIGN KEY (securities_id) REFERENCES `securities` (`securities_id`), 
  ADD FOREIGN KEY (quote_id) REFERENCES `quotes` (`quote_id`); 